package lists;

import dates.*;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;


public class PickDate extends MyDriver 
{ 
	final List<String> monthList = Arrays.asList("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"); 
	// Expected Date, Month and Year
	int expMonth;
	int expMonthUt;
	int expYear;
	String expDate;
	// Last Date, Month and Year
	String endDate;
	int endMonthUt;
	int endYear;
	int endMonth;
	// Calendar Month and Year
	String calMonthSelectedNow = null;
	String calYearSelectedNow = null;
	String urlsFree;
	String urlsPaid;
	String urlsGrossing;
	StringCsv StringFPG = new StringCsv(this.urlsFree, this.urlsPaid, this.urlsGrossing);


	public PickDate() throws IOException 
	{
		super();
		this.urlsFree = "";
		this.urlsPaid = "";
		this.urlsGrossing = "";
		this.expDate = "13";
		this.expMonthUt= 04;
		this.expYear = 2016;
		this.expMonth=this.expMonthUt-1;
		//Set your expected date, month and year.
		this.endDate = "27";
		this.endMonthUt = 04;
		this.endYear = 2015;
		this.endMonth=this.endMonthUt-1;
	}


	public void pickExpDate(String url) throws InterruptedException, IOException
	{
		boolean dateNotFound = true;
		Calendar calExpToStart = Calendar.getInstance();
		Calendar calEnd = Calendar.getInstance();
		Calendar calToday = Calendar.getInstance();
		List<WebElement> elements;	// used to put all elements of webpage to select the date from calendar
		List<WebElement> webElementsMonthsFromSiteCal;
		List<WebElement> webElementsYearsFromSiteCal;
		WebElement elementMonth;
		WebElement elementYear;
		WebElement element = null;
		calExpToStart=calToday;
		//calExpToStart.set(this.expYear, this.expMonth, Integer.parseInt(this.expDate)); // to use to input start date (expYear, expMonth, expDate) comment previous row
		calEnd.set(this.endYear, this.endMonth, Integer.parseInt(this.endDate));
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy MMM dd");
		System.out.println("Checking from " + sdf.format(calExpToStart.getTime()) + "to " + sdf.format(calEnd.getTime()));
		do
		{
			System.out.println("Serching correct date");
			element = this.findElement(By.className("dashboard-meta"));
			//System.out.println(element);
			elements = element.findElements(By.cssSelector("div"));
			//		System.out.println("Entrato nel Do. Data appena inserita: " + sdf.format(calExpToStart.getTime()));
			//System.out.println(elements);
			int counter = 0;
			for (WebElement ele: elements)
			{
				//System.out.println(ele);
				counter++;
				if(counter==6)	// date
				{
					ele.click();
					break;
					//elementDate = ele;
				}
			} 
			//This loop will be executed continuously till dateNotFound Is true.	
			while(dateNotFound)
			{
				//first thing: searching the start date on the calendare of website
				elementMonth = this.findElement(By.className("ui-datepicker-month"));
				//System.out.println(elementMonth);
				webElementsMonthsFromSiteCal = elementMonth.findElements(By.cssSelector("option"));
				//System.out.println(webElementsMonthsFromSiteCal);

				//System.out.println("for webelement");
				for (WebElement ele : webElementsMonthsFromSiteCal)
				{
					//System.out.println(ele.getText());
					if (ele.isSelected())
					{
						//System.out.println(ele.getText());
						this.calMonthSelectedNow = ele.getText();
						break;
					}
				}
				//System.out.println("selected this.calMonthSelectedNow: " + this.calMonthSelectedNow);
				//System.out.println("end for webelement");
				//System.out.println("this.calMonthSelectedNow" + this.calMonthSelectedNow);
				//Retrieve current selected year name from date picker popup.
				elementYear = this.findElement(By.className("ui-datepicker-year"));
				webElementsYearsFromSiteCal = elementYear.findElements(By.cssSelector("option"));
				for (WebElement ele : webElementsYearsFromSiteCal)
				{
					//System.out.println(ele.getText());
					if (ele.isSelected())
					{
						//System.out.println(ele.getText());
						this.calYearSelectedNow = ele.getText();
						break;
					}
				}
				//System.out.println("selected this.calYearSelectedNow: " + this.calYearSelectedNow);
				//System.out.println("selected calExpToStart: " + calExpToStart.get(Calendar.YEAR));
				//System.out.println("selected this.calMonthSelectedNow: " + this.calMonthSelectedNow);
				//System.out.println("selected calExpToStart: " + calExpToStart.get(Calendar.MONTH));
				//If current selected month and year are same as expected month and year then go Inside this condition.
				if(this.monthList.indexOf(this.calMonthSelectedNow) == calExpToStart.get(Calendar.MONTH) && (calExpToStart.get(Calendar.YEAR) == Integer.parseInt(this.calYearSelectedNow))) 
				{
					//Month and year are matched. Now Call selectDate function with date to select and set dateNotFound flag to false.
					//System.out.println("matched:\ncalget month: " + calExpToStart.get(Calendar.MONTH) + " = index this.calMonthSelectedNow: " + this.calMonthSelectedNow + "\ncalget year: " + calExpToStart.get(Calendar.YEAR) + " = calyear: " + this.calYearSelectedNow);
					StringFPG.monthYearForNameList = this.calYearSelectedNow + "_" + (this.monthList.indexOf(this.calMonthSelectedNow)+1) + "_";
					this.selectDate(String.valueOf(calExpToStart.get(Calendar.DAY_OF_MONTH)));
					dateNotFound = false; 
				}
				//If current selected month and year are less than expected start month and year then go Inside this condition.
				else if(this.monthList.indexOf(this.calMonthSelectedNow) < calExpToStart.get(Calendar.MONTH) && (calExpToStart.get(Calendar.YEAR) == Integer.parseInt(this.calYearSelectedNow)) || calExpToStart.get(Calendar.YEAR) > Integer.parseInt(this.calYearSelectedNow)) 
				{
					//Click on next button of date picker.
					this.findElement(By.className(".ui-datepicker-next")).click();
				} 
				//If current selected month and year are greater than expected start month and year then go Inside this condition.
				else if(this.monthList.indexOf(this.calMonthSelectedNow) > calExpToStart.get(Calendar.MONTH) && (calExpToStart.get(Calendar.YEAR) == Integer.parseInt(this.calYearSelectedNow)) || calExpToStart.get(Calendar.YEAR) < Integer.parseInt(this.calYearSelectedNow))
				{ 
					//				System.out.println("\nthis.monthList.indexOf(this.calMonthSelectedNow)+1:" + (this.monthList.indexOf(this.calMonthSelectedNow)) + "\nthis.expMonth:" + calExpToStart.get(Calendar.MONTH));
					//				System.out.println("\nInteger.parseInt(this.calYearSelectedNow):" +  Integer.parseInt(this.calYearSelectedNow)  + "\nthis.expYear: " +  calExpToStart.get(Calendar.YEAR));
					//Click on prev button of date picker.
					this.findElement(By.className("ui-datepicker-prev")).click();
				}
			}
			Thread.sleep(3000); 
			calExpToStart.add(Calendar.DATE, (-14));	// to go back 14 days before of selected date.
			dateNotFound=true;
			//			System.out.println(sdf.format(calExpToStart.getTime()));
			//			System.out.println("data on normal variables: giorno: " + this.expDate +" mese: " + this.expMonth + " anno: " + this.expYear);
		}
		while(!(calExpToStart.compareTo(calEnd)<0));	// it goes out from while only if, going back, calExpToStart becames older then calEnd 
	} 


	public void selectDate(String date) throws IOException
	{
		List<WebElement> datePicker = this.findElements(By.className("ui-state-default"));
		for ( WebElement ele: datePicker)
		{
			StringCsv.dataForNameList=ele.getText();
			//System.out.println(ele.getText());
			if (StringCsv.dataForNameList.equals(date))
			{	
				ele.click();
				this.takeList();
				break;	
			}	
		}
		//StringFPG = new StringCsv(this.urlsFree, this.urlsPaid, this.urlsGrossing);
		StringFPG.myStringFree=this.urlsFree;
		this.urlsFree="";
		StringFPG.myStringPaid=this.urlsPaid;
		this.urlsPaid="";
		StringFPG.myStringGrossing=this.urlsGrossing;
		this.urlsGrossing="";
		StringFPG.writeListsToCsv();
		//System.out.println("ends select date");
	}


	public void takeList()
	{
		System.out.println("Extracting lists");
		int counter = 0;
		List<WebElement> elements = this.findElementById("storestats-top-table").findElements(By.className("title-link"));
		for (WebElement ele : elements)
		{
			counter++;
			if (counter%3 == 1) // free
				this.urlsFree=this.urlsFree.concat(ele.getAttribute("href") + "\n");
			if (counter%3 == 2) // paid
				this.urlsPaid=this.urlsPaid.concat(ele.getAttribute("href") + "\n");
			if (counter%3 == 0)  // grossing
				this.urlsGrossing=this.urlsGrossing.concat(ele.getAttribute("href") + "\n");
		}	
	}

}
