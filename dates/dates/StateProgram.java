package dates;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.openqa.selenium.NoSuchElementException;

public class StateProgram 
{
	List<String> listUrlDone;
	String lastAppPrevSession;

	public StateProgram() 
	{
		this.listUrlDone=new ArrayList<String>();
	}

	public List<String> restoreState() 
	{
		int size = listUrlDone.size();
		if (size>1)
		{
			MyProgram.justRestored=true;
			MyProgram.nPagesVisited=size;
			return this.listUrlDone;
		}
		return new ArrayList<String>();
	}

	public void readState() throws IOException 
	{

		String fileInput = "./Input_Output/Apps_info_taken/dates_Output.csv";
		File file = new File(fileInput);
		PrintWriter writer;
		StringBuilder fileContent = new StringBuilder();
		try
		{
			Scanner inputStream = new Scanner(file);
			

			while(inputStream.hasNextLine())
			{
				//read single line, put in string
				String data  = inputStream.nextLine();
				if (inputStream.hasNextLine() )
				{
				fileContent.append(data + System.getProperty("line.separator"));
				String[] dataVet = data.split("\t");
				//System.out.println(data);
				this.listUrlDone.add(dataVet[0]);	// add each url to myListInput
				}		
			}
			writer = new PrintWriter(new FileWriter("./Input_Output/Apps_info_taken/" + "dates_Output.csv"));
			writer.print(fileContent);
			
			//System.out.println(listUrlDone);

			inputStream.close();
			writer.close();
		}
		catch (FileNotFoundException noState)
		{
		}
	}
}



