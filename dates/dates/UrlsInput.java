package dates;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

public class UrlsInput 
{
	List<String> myListInput = new ArrayList<String>();
	int fileNumber;
	public static List<String> listNamesFiles = new ArrayList<String>();
	public static int indexFile;
	LinkedHashSet<String> mySetInput = new LinkedHashSet<>();


	public UrlsInput() 
	{
	}


	public List<String> inputFromCsvFile()
	{
		System.out.print("Import Input urls from input.csv... ");
		String fileInput = "input.csv";
		File file = new File(fileInput);
		try
		{
			// -read from input.csv with Scanner class
			Scanner inputStream = new Scanner(file);
			// hashNext() loops line-by-line
			while(inputStream.hasNext())
			{
				//read single line, put in string
				String data  = inputStream.next();
				//System.out.println(data);
				this.myListInput.add(data);	// add each url to myListInput
			}
			inputStream.close();
			System.out.print("Websites to visit: ");
			//System.out.println(this.myListInput);
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		System.out.println(MyProgram.OK);

		return this.myListInput;
	}


	public List<String> inputFromCsvMoreFiles()
	{
		fileNumber = 0;
		System.out.print("Import Input urls from csv files... ");
		String pathInput = "./Input_Output/Url_lists/";
		File file = new File(pathInput);
		File[] files = file.listFiles();
		Arrays.sort(files, Collections.reverseOrder());
		try
		{
			// -read fromfile csv with Scanner class
			for (File eachFile : files) 
			{
				fileNumber++;
				UrlsInput.listNamesFiles.add(eachFile.getName()); 
				Scanner inputStream = new Scanner(eachFile);
				String data = null;
				while(inputStream.hasNext())
				{
					//read single line, put in string
					data  = inputStream.next();
					System.out.println(data);
					this.myListInput.add(data);	// add each url to myListInput
				}
				if(!inputStream.hasNext())	
				{
					this.myListInput.add("end_File\n");
				}
				inputStream.close();  
			}



			//System.out.println("Websites to visit: " + this.myListInput);
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		System.out.println(MyProgram.OK);

		return this.myListInput;

	}

	public Set<String> deleterDuplicates() throws IOException
	{
		fileNumber = 0;
		MyProgram.message = "Import Input urls from csv files... ";
		System.out.print(MyProgram.message);
		String pathInput = "./Input_Output/Url_lists/";
		File file = new File(pathInput);
		File[] files = file.listFiles();
		Arrays.sort(files, Collections.reverseOrder());
		try
		{
			// -read fromfile csv with Scanner class
			for (File eachFile : files) 
			{
				fileNumber++;
				UrlsInput.listNamesFiles.add(eachFile.getName()); 
				Scanner inputStream = new Scanner(eachFile);
				String data = null;
				while(inputStream.hasNext())
				{
					//read single line, put in string
					data  = inputStream.next();
					this.mySetInput.add(data);	// add each url to myListInput
				}
				inputStream.close();  
			}

			CountTime.getElapsedTimeSecs(MyProgram.message + MyProgram.OK);
			System.out.println(MyProgram.OK);

			System.out.println("n of urls without repetitions: " + this.mySetInput.size());



			//System.out.println("Websites to visit in set: " + this.mySetInput);
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}

		return this.mySetInput;

	}


	public List<String> setToList()
	{

		this.myListInput.clear();
		this.myListInput.addAll(this.mySetInput);
		//System.out.println("Websites to visit in list: " + this.myListInput);
		return this.myListInput;
	}

}


