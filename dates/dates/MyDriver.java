package dates;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;


public class MyDriver extends FirefoxDriver 
{
	public  String baseUrl;		// string to save the website url it has been visiting
	String absoluteImageUrl;
	String relativeImageUrl;
	String fileName = "data";	// string to give the name to html file of downloaded webpage
	public int appMiss;
	static Integer fileNameNum = 1;	// int to give a sequential name to html file of downloaded webpage

	/**
	 * @param args
	 * @throws IOException 
	 */

	public MyDriver() throws IOException
	{
	}


	public MyDriver login() throws IOException
	{
		this.manage().timeouts().implicitlyWait(8, TimeUnit.SECONDS); // Wait For Page To Load
		this.controlsIfConnectionIsOk();

		if (MyProgram.nPagesVisited == 0 || MyProgram.justRestored )	// control to understand if it is the first page (and it's necessary the log in) or not
		{

			MyProgram.message = "Logging... ";
			System.out.print(MyProgram.message);
			this.findElement(By.id("email")).sendKeys(new String[] { "foxmat90@gmail.com" }); // Enter user
			this.findElement(By.id("password")).sendKeys(new String[] { "3a1265ctfNt1321" }); // Enter Password
			this.findElement(By.id("submit")).click(); // Click on 'Sign In' button

			CountTime.getElapsedTimeSecs(MyProgram.message);
			System.out.println(MyProgram.OK);
		}

		return this;
	}


	public boolean controlsIfConnectionIsOk()
	{
		List<WebElement> error = this.findElementsById("errorPageContainer");
		//this.manage().timeouts().implicitlyWait(8, TimeUnit.SECONDS);
		if (error.size()!=0)
		{
			System.out.println("Error Connession");
			System.exit(1);
			return false;
		}
		else
		{
			return true;
		}

	}

	public String findImagesRelativeUrl() throws IOException
	//	method to find images with relative url and save it to replace them with the absolute url
	//	to can show when it is opens download webpage. N.B. the replacement'll be made in "downloadPage method
	{
		this.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		MyProgram.message = "finding images with relative url... ";
		System.out.print(MyProgram.message);
		absoluteImageUrl = "absoluteImageUrlNotInitialize";
		relativeImageUrl = "relativeImageUrlNotInitialize";
		String rootWebSite = "https://www.appannie.com";
		String[] vett;	// vett is necessary to use split method to save relative url and absolute url separatly
		List<WebElement> elements = this.findElements(By.cssSelector("img"));
		if(elements.size()!=0)
		{
			String stringSrc;
			for(WebElement ele : elements)
			{
				stringSrc=ele.getAttribute("src");
				if (stringSrc.startsWith(rootWebSite ))	// it give the absolute url although the url is relative
				{
					this.absoluteImageUrl = stringSrc;
					//System.out.println(this.absoluteImageUrl);
					vett = this.absoluteImageUrl.split(rootWebSite); //in this way I take only the relative part without the root of website
					this.relativeImageUrl=vett[1];
					//System.out.println( "relativeimage = " + vett[1]);
				}
			}
		}
		CountTime.getElapsedTimeSecs(MyProgram.message + MyProgram.OK);
		System.out.println(MyProgram.OK);

		return this.relativeImageUrl;
	}


	public boolean appExists() throws IOException
	// it controls if the app's web page exists or not
	{
		this.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		MyProgram.message="checking if app esists yet... ";
		System.out.print(MyProgram.message);
		boolean exist = false;
		try
		{
			exist = this.findElementByClassName("main-app-content").findElements(By.cssSelector(".detail-container")).size()!=0;
			//this.manage().timeouts().implicitlyWait(8, TimeUnit.SECONDS);
		}
		catch(NoSuchElementException fewVersions)
		{
			this.controlsIfConnectionIsOk();
		}
		//this.manage().timeouts().implicitlyWait(8, TimeUnit.SECONDS);
		if(exist)
		{
			CountTime.getElapsedTimeSecs(MyProgram.message + MyProgram.OK);
			System.out.println(MyProgram.OK);	
		}
		else
		{
			CountTime.getElapsedTimeSecs(MyProgram.message + " it doesn't esist");
			System.out.println(" it doesn't esist");
		}

		return exist;
	}


	public MyDriver downloadPage() throws IOException
	// method to download the page and the relative urls are replaced with absolute ones
	{
		this.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		String[] fileName = this.baseUrl.split("https://www.appannie.com/apps/ios/app/");
		fileName = fileName[1].split("/details/");
		this.fileName = fileName[0];
		String pathOutput = "./Input_Output/HTML_pages/" + this.fileName + ".html";
		File file = new File(pathOutput);
		if (file.exists())

			return this;
		else
		{
			//SSystem.out.println("non esiste");
			MyProgram.message="Downloading webpage on data.html... ";
			System.out.print(MyProgram.message);
			PrintWriter out = new PrintWriter(pathOutput);	// sequential name for each html file
			MyDriver.fileNameNum++;
			String code = this.getPageSource();	// it gives the page's code
			//System.out.println(this.absoluteImageUrl);
			//System.out.println(this.relativeImageUrl);
			code = code.replaceAll(this.relativeImageUrl, this.absoluteImageUrl);	//replacement from relative to absolute
			out.println(code);
			//System.out.println(code);   
			out.close();
			CountTime.getElapsedTimeSecs(MyProgram.message + MyProgram.OK);
			System.out.println(MyProgram.OK);

			return this;
		}
	}


	public String getDescriptionProduct() throws IOException
	{
		this.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		//method to obtain description from html code, prepares it, show it and call method to write it on csv write
		MyProgram.message="Fetching description's product... ";
		System.out.print(MyProgram.message);
		String description = "";	// init string to storage the description part waiting to write it on the csv file
		String[] vett;
		List<WebElement> elements;	// list of webelement to storage each webelement (paragraph)
		WebElement elem;
		By bySearch = By.className("app_content_section");
		elements = this.findElements(bySearch);
		try
		{
			elem = this.findElementById("app_content");

			//this.manage().timeouts().implicitlyWait(8, TimeUnit.SECONDS);

			//this.manage().timeouts().implicitlyWait(8, TimeUnit.SECONDS);
			try
			{
				this.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
				WebElement elementVersionsClick = elem.findElement(By.className("read_more")).findElement(By.cssSelector("a"));	
				//this.manage().timeouts().implicitlyWait(8, TimeUnit.SECONDS);
				elementVersionsClick.click();
				this.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			}
			catch (NoSuchElementException fewVersions)
			{
				this.controlsIfConnectionIsOk();
			}
		}
		catch (NoSuchElementException fewVersions)
		{
			this.controlsIfConnectionIsOk();
		}
		if (elements.size()!=0)
		{
			for (WebElement ele : elements)
			{
				//System.out.println(ele.getText());
				//System.out.println("css" + ele.findElement(By.cssSelector("h3")).getText());
				try
				{
					if (ele.findElement(By.cssSelector("h3")).getText().equals("Description"))
					{
						//System.out.println(description.concat(ele.getText()));
						description = description.concat(ele.getText());
					}
				}
				catch (NoSuchElementException fewVersions)
				{
					this.controlsIfConnectionIsOk();
				}
			}
		}
		vett = description.split(System.lineSeparator() + System.lineSeparator() + System.lineSeparator()); // it deletes breaks line not necessary at the end of dexcription
		description=vett[0];
		//System.out.println("Description:" + description);
		description = description.replace("\n", " ").replace("\r", " ");	// it removes breaks line not necessary among paragraphs
		StringCsv myStringDescription = new StringCsv(description);
		myStringDescription.writeDatesToCsv(0, 0);	// method to write on csv file
		CountTime.getElapsedTimeSecs(MyProgram.message + MyProgram.OK);
		System.out.println(MyProgram.OK);

		return description;
	}


	public void getVersionsProduct() throws IOException
	//method to obtain versions from html code, prepares them, show them and call method to write them on csv write
	{
		this.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		List<WebElement> elementsVersions = null;
		String dateVer = null;
		String noteVer = null;
		String nVer = null;
		List <String> versions = new ArrayList<String>();
		String version = "";
		String versionLast = " ";
		StringCsv myStringCsv = null;
		MyProgram.message="Fetching versions's product...";
		System.out.print(MyProgram.message);
		try
		{
			WebElement elementVersions = this.findElement(By.id("app_versions"));

			//this.manage().timeouts().implicitlyWait(8, TimeUnit.SECONDS);

			elementsVersions = elementVersions.findElements(By.cssSelector("tr"));

			//System.out.println(elementVersions.getText());
			this.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
			try
			{
				System.out.print("first try ... ");
				WebElement elementVersionsClick = elementVersions.findElement(By.cssSelector("b"));
				//this.manage().timeouts().implicitlyWait(8, TimeUnit.SECONDS);
				elementVersionsClick.click();
			}
			catch (NoSuchElementException fewVersions)
			{
				this.controlsIfConnectionIsOk();
			}



			CountTime.getElapsedTimeSecs(MyProgram.message + MyProgram.OK);
			System.out.println(MyProgram.OK);
			System.out.print("second try ... ");

			for (WebElement ele : elementsVersions)
			{
				try
				{
					this.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
					elementVersions= ele.findElement(By.className("version-text"));
					nVer = elementVersions.getText();
					elementVersions= ele.findElement(By.className("date"));
					dateVer = elementVersions.getText();
					elementVersions= ele.findElement(By.className("note"));
					noteVer = elementVersions.getText();
				}
				catch (NoSuchElementException fewVersions)
				{
					this.controlsIfConnectionIsOk();
				}
				version = nVer + " " + dateVer + " " + noteVer;
				if(!version.equals(versionLast))
				{

					versions.add(version);
					versionLast=version;
				}
			}
			CountTime.getElapsedTimeSecs(MyProgram.message + MyProgram.OK);
			System.out.println(MyProgram.OK);
			//System.out.println("Versions: " + versions);
			for (String oneVersion : versions)
			{
				myStringCsv = new StringCsv(oneVersion);
				myStringCsv.writeDatesToCsv(0, 0);	// method to write on csv file
			}
			CountTime.getElapsedTimeSecs(MyProgram.message + MyProgram.OK);
			System.out.println(MyProgram.OK);

		}
		catch (NoSuchElementException fewVersions)
		{	
			this.controlsIfConnectionIsOk();
		}
	}


	public MyDriver OpenWebsite(String eachUrl, int ch) throws IOException, InterruptedException
	// method to open a website url
	{
		//this.manage().window().maximize();
		this.controlsIfConnectionIsOk();
		this.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		MyProgram.message="Opening webpage: ";
		System.out.print(MyProgram.message + eachUrl + " ... ");
		Thread.sleep(500);
		this.baseUrl=eachUrl;
		this.get(this.baseUrl);
		StringCsv myStringUrl = new StringCsv(this.baseUrl);
		if (ch==0)
			myStringUrl.writeUrlToCsv();	// method to write url on csv file
		else
		{
		}
		CountTime.getElapsedTimeSecs(MyProgram.message + MyProgram.OK);
		System.out.println(MyProgram.OK);
		return this;
	}


	public MyDriver closeAndOpenTab(UrlsInput nextUrl) throws InterruptedException, IOException
	// // method to open a new tab and close the old one
	{
		this.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		this.controlsIfConnectionIsOk();
		MyProgram.message="Opening new tab browser to visit the next url... ";
		MyProgram.nPagesVisited++;
		if (nextUrl.myListInput.lastIndexOf(this.baseUrl) + 1 < nextUrl.myListInput.size()) // control we are at the last element of the list or not
		{
			System.out.print(MyProgram.message);
			this.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL +"t");	// it opens a new tab
			//this.manage().timeouts().implicitlyWait(8, TimeUnit.SECONDS);
			Thread.sleep(1000); 
			this.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL, Keys.TAB); // it passes to the old tab
			//this.manage().timeouts().implicitlyWait(8, TimeUnit.SECONDS);
			this.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL +"w");	// it closes old tab
			//this.manage().timeouts().implicitlyWait(8, TimeUnit.SECONDS);

		}	
		CountTime.getElapsedTimeSecs(MyProgram.message + MyProgram.OK);
		System.out.println(MyProgram.OK);
		return this;			
	}

}
