package dates;

import java.io.IOException;
import org.openqa.selenium.remote.UnreachableBrowserException;


public class MyProgram 
{

	static protected int nPagesVisited;
	static final String OK = " OK";
	static String message = "";
	static boolean justRestored;
	/**
	 * @param args
	 * @throws IOException  
	 * @throws InterruptedException 
	 * @throws UnreachableBrowserException
	 */


	public MyProgram() throws IOException 
	{
		nPagesVisited = 0;
		CountTime.myStart();
		MyProgram.justRestored = false;
	}


	public void execute()
	{
	}


	public void executeDates() throws IOException, InterruptedException
	{

		System.out.println("Start");
		UrlsInput myUrlsInput = new UrlsInput();
		//myUrlsInput.inputFromCsvMoreFiles();	// extract the list of urls from csv file 
		myUrlsInput.deleterDuplicates();
		myUrlsInput.setToList();
		StateProgram myStateProgram = new StateProgram();
		myStateProgram.readState();
		myUrlsInput.myListInput.removeAll(myStateProgram.restoreState());
		System.out.println("n of urls without repetitions after restore: " + myUrlsInput.myListInput.size());
		if (MyProgram.nPagesVisited == 0)
			StringCsv.writeHeader(0);  
		System.out.print("Opening browser... ");
		MyDriver driver1 = new MyDriver();
		System.out.println("OK");
		//System.out.println(myUrlsInput.myListInput.size());
		for(String eachUrl : myUrlsInput.myListInput) 		// loop to process each url of list
		{
			if (eachUrl != "end_File\n")	//normal, not end file
			{
				driver1.OpenWebsite(eachUrl, 0);
				driver1.login();
				if (driver1.appExists())
				{
					driver1.findImagesRelativeUrl();	// change relative url images to absolute url images on the webpage to show them then downloaded 
					driver1.downloadPage();
					driver1.getDescriptionProduct();
					driver1.getVersionsProduct();
				}
				driver1.closeAndOpenTab(myUrlsInput);	// open new tab with next url and close the old one. In this way it's not necessary to log in again
			}
			else	// end file
			{
				//UrlsInput.indexFile++;
				//StringCsv.writeHeader(0);    
			}
			MyProgram.justRestored = false;
		}
		driver1.close();
		CountTime.myStop();;
		System.out.println("End"); 
	}

	public void executeLists()
	{
		System.out.println("Start");


	}


}

