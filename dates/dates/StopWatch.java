package dates;

import java.util.concurrent.ConcurrentHashMap;

public class StopWatch 
{

	public static ConcurrentHashMap<Long, Long> histogram = new ConcurrentHashMap<Long, Long>();

	/**
	 * Creates an instance of the timer and starts it running.
	 */
	public static StopWatch getInstance() 
	{
		return new StopWatch();
	}

	private long end = -1;
	private long interval = -1;
	private final long start;

	private StopWatch() 
	{
		start = interval = currentTime();
	}

	/**
	 * Returns in milliseconds the amount of time that has elapsed since the timer was created. If the
	 * <code>stop</code> method has been invoked, then this returns instead the elapsed time between the creation of
	 * the timer and the moment when <code>stop</code> was invoked.
	 * 
	 * @return duration it took
	 */
	public long getDuration() 
	{
		long result = 0;

		final long startTime = start;
		final long endTime = isTimerRunning() ? currentTime() : end;

		result = nanoToMilliseconds(endTime - startTime);

		boolean done = false;
		while (!done) 
		{
			Long oldValue = histogram.putIfAbsent(result, 1L);
			if (oldValue != null) 
			{
				done = histogram.replace(result, oldValue, oldValue + 1);
			} else 
			{
				done = true;
			}
		}

		return result;
	}

	/**
	 * Returns in milliseconds the amount of time that has elapsed since the last invocation of this same method. If
	 * this method has not previously been invoked, then it is the amount of time that has elapsed since the timer
	 * was created. <strong>Note</strong> that once the <code>stop</code> method has been invoked this will just
	 * return zero.
	 * 
	 * @return interval period
	 */
	public long getInterval() 
	{
		long result = 0;

		final long startTime = interval;
		final long endTime;

		if (isTimerRunning()) 
		{
			endTime = interval = currentTime();
		}
		else 
		{
			endTime = end;
		}

		result = nanoToMilliseconds(endTime - startTime);

		return result;
	}

	/**
	 * Stops the timer from advancing. This has an impact on the values returned by both the
	 * <code>getDuration</code> and the <code>getInterval</code> methods.
	 */
	public void stop() 
	{
		if (isTimerRunning()) 
		{
			end = currentTime();
		}
	}

	/**
	 * What is the current time in nanoseconds?
	 * 
	 * @return returns back the current time in nanoseconds
	 */
	private long currentTime() 
	{
		return System.nanoTime();
	}

	/**
	 * This is used to check whether the timer is alive or not
	 * 
	 * @return checks whether the timer is running or not
	 */
	private boolean isTimerRunning() 
	{
		return (end <= 0);
	}

	/**
	 * This is used to convert NanoSeconds to Milliseconds
	 * 
	 * @param nanoseconds
	 * @return milliseconds value of nanoseconds
	 */
	private long nanoToMilliseconds(final long nanoseconds)
	{
		return nanoseconds / 1000000L;
	}
}