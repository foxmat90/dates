package dates;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.Scanner;

public class CountTime
{

	static private long startTime = 0;
	static private long stopTime = 0;
	static private boolean running = false;
	static private FileWriter timeFile;
	static private PrintWriter printFile;
	static private float timeLastTask=0;
	static private float timeLastApp=0;
	static private float lastElapsedForTask=0;
	static private float lastElapsedForApp=0;
	static private float elapsedRestorePast;
	static private float elapsedRestoreFuture;
	static private FileWriter timeFileRestore;
	static private PrintWriter printFileRestore;
	static private boolean restored;



	static public void myStart() throws IOException 
	{
		//CountTime.timeFile = new FileWriter("./Input_Output/timeFile.csv", true);


		String fileInputTimeRestore = "./Input_Output/timeFileRestore.csv";
		File file = new File(fileInputTimeRestore);
		try
		{
			// -read from input.csv with Scanner class
			Scanner inputStream = new Scanner(file);
			String data  = inputStream.next();
			System.out.print("letto: " + data);
			CountTime.elapsedRestorePast=Float.valueOf(data);
			CountTime.restored=true;
			inputStream.close();

		}
		catch (FileNotFoundException e)
		{
			CountTime.elapsedRestorePast=0;
			CountTime.restored=false;
		}

		CountTime.startTime = (long) (System.currentTimeMillis());
		CountTime.running = true;
	}


	static public void myStop()
	{
		CountTime.stopTime = System.currentTimeMillis();
		CountTime.running = false;
	}



	//elaspsed time in seconds
	static public float getElapsedTimeSecs(String timeDescription) throws IOException
	{
		float elapsed;
		CountTime.timeFile = new FileWriter("./Input_Output/timeFile.csv", true);
		CountTime.printFile = new PrintWriter(CountTime.timeFile);

		CountTime.timeFileRestore = new FileWriter("./Input_Output/timeFileRestore.csv");
		CountTime.printFileRestore = new PrintWriter(CountTime.timeFileRestore);

		if (CountTime.restored)
		{
			if (running)
			{
				elapsed = ((float)(System.currentTimeMillis() - CountTime.startTime) / 1000) + CountTime.elapsedRestorePast;
			}
			else
			{
				elapsed = ((float)(CountTime.stopTime - CountTime.startTime) / 1000) + CountTime.elapsedRestorePast;
			}
		}
		else
		{
			if (running)
			{
				elapsed = ((float)(System.currentTimeMillis() - CountTime.startTime) / 1000);
			}
			else
			{
				elapsed = ((float)(CountTime.stopTime - CountTime.startTime) / 1000);
			}
		}
		System.out.print("Total time: " + elapsed + " sec. ");

		CountTime.timeLastTask=(float)elapsed-CountTime.lastElapsedForTask;
		CountTime.lastElapsedForTask=(float)elapsed;

		System.out.print("Task time: " + CountTime.timeLastTask + " sec. ");
		CountTime.printFile.println(timeDescription + "\t" + "Total time: " + elapsed + " sec. " + "\t" + "Task time: " + CountTime.timeLastTask + " sec. ");
		if (timeDescription.contains("Opening new tab browser to visit the next url"))
		{
			CountTime.timeLastApp=(float)elapsed-CountTime.lastElapsedForApp;
			CountTime.lastElapsedForApp=(float)elapsed;
			System.out.print("\nApp time: " + CountTime.timeLastApp + " sec. ");
			CountTime.printFile.println("App time: " + CountTime.timeLastApp + " sec. ");
		}

		CountTime.elapsedRestoreFuture=elapsed;

		//System.out.print("elapsedRestore " + CountTime.elapsedRestore);
		CountTime.printFileRestore.println(CountTime.elapsedRestoreFuture);
		CountTime.printFileRestore.close();
		CountTime.printFile.close();

		return elapsed;
	}



}
