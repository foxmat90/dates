package dates;


import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import lists.OnlyListUrls;


public class StringCsv
{
	public final static String FILE_HEADER = "URL\tDESCRIPTION\tVERSIONS\t";
	public final static String FILE_HEADER_LIST = "URL\tAPP\tPUBLISHER\tDELTA POS\tAPP IN APP PURCHASES\tLINK APPANNIE\tLINK MARKET\n\t";
	public String myString; // string where text from needed website's parts is put waiting to write it on csv file
	public String fileName;
	public String objectName;
	public String myStringFree;
	public String myStringPaid;
	public String myStringGrossing;
	public static String dataForNameList;
	public static String monthYearForNameList;



	public StringCsv(String myString) 
	{
		this.myString = myString;
	}


	public StringCsv(String myStringFree, String myStringPaid, String myStringGrossing) 
	{
		this.myStringFree = myStringFree;
		this.myStringPaid = myStringPaid;
		this.myStringGrossing = myStringGrossing;
	}


	public static void writeHeader(int ch) throws IOException
	// method to write table's header on csv file
	{
		String message = "Preparing csv file... ";
		System.out.print(message);
		//PrintWriter writer = new PrintWriter(new FileWriter("./Input_Output/Apps_info_taken/" + "dates_" + UrlsInput.listNamesFiles.get(UrlsInput.indexFile)));
		PrintWriter writer = new PrintWriter(new FileWriter("./Input_Output/Apps_info_taken/" + "dates_Output.csv"));
		if (ch==0)
			writer.print(StringCsv.FILE_HEADER);
		else
			writer.print(StringCsv.FILE_HEADER_LIST);
		writer.close();
		CountTime.getElapsedTimeSecs(message + MyProgram.OK);
		System.out.println(MyProgram.OK);
	}


	public void writeDatesToCsv(int select, int select2) throws IOException
	// method to write text on csv file from needed website's parts 
	{
		PrintWriter writer;
		//System.out.println("Writing on csv file...");
		this.fileName = this.fileOutputNamer(select2, OnlyListUrls.urlToVisit); //date+country+(free o paid o grossing) al posto di virgolette
		if(select2 == 0)
			writer = new PrintWriter(new FileWriter("./Input_Output/Apps_info_taken/" + "dates_Output.csv", true));
		else
			writer = new PrintWriter(new FileWriter("./Input_Output/Apps_info_taken/" + "dates_Output.csv"));	
		if(select == 0)
			writer.append("\t");
		else
		{
		}	
		writer.append(this.myString);
		writer.close();
	}


	public void writeUrlToCsv() throws IOException
	// method to write url of a webpage on csv file
	{
		PrintWriter writer = new PrintWriter(new FileWriter("./Input_Output/Apps_info_taken/" + "dates_Output.csv", true)); // da usare fileOutputName
		if (MyProgram.justRestored == false)
			writer.append("\n");
		writer.append(this.myString);
		writer.close();
	}


	public String fileOutputNamer(int select, String rif) throws IOException

	{
		switch (select)
		{
		case 0: 

			return "dates_" + UrlsInput.listNamesFiles.get(UrlsInput.indexFile);
		case 1:
			if (rif.contains("china"))
			{

				return (StringCsv.monthYearForNameList + StringCsv.dataForNameList + "_" + "china" + "_" + this.objectName + ".csv");
			}
			else

				return (StringCsv.monthYearForNameList + StringCsv.dataForNameList + "_" + "usa" + "_" + this.objectName + ".csv");

		case 2:

			return rif;

		}

		return rif;
	}


	public void writeListsToCsv() throws IOException
	// method to write text on csv file from needed website's parts 
	{
		PrintWriter writer;
		System.out.println("Writing list on csv file...");
		int i;
		for(i=0; i<3; i++)
		{
			if(i==0)
				this.objectName="free";
			if(i==1)
				this.objectName="paid";
			if(i==2)
				this.objectName="grossing";
			this.fileName = this.fileOutputNamer(1, OnlyListUrls.urlToVisit); //date+country+(free o paid o grossing) al posto di virgolette
			writer = new PrintWriter(new FileWriter("./Input_Output/Url_lists/" + this.fileName));
			if(i==0)
				writer.append(this.myStringFree);
			if(i==1)
				writer.append(this.myStringPaid);
			if(i==2)
				writer.append(this.myStringGrossing);
			writer.close();
		}
	}

}
