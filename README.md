# README #

I developed a java program to extract, from some webpages, data about features and spread of most important mobile apps in different countries market.
This is the result of my experience at Bournemouth University where I worked for 4 months within the Erasmus traineeship program.
My thesis and discussion were based on this work.
Thesis: https://www.dropbox.com/s/cc5fdc03f1te56f/Thesis.pdf?dl=0
Slides for the discussion: https://www.dropbox.com/s/5f9cry1jg7o3pty/Mattia_Palla_48971%5B1%5D.pptx?dl=0